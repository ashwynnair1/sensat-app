import { Component } from '@angular/core';
import { SensorInterface } from './core/models/sensor.interface';
import { BehaviorSubject } from 'rxjs';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  sensorOnMap: BehaviorSubject<SensorInterface> = new BehaviorSubject<SensorInterface>(null);
  tableHeight: BehaviorSubject<number> = new BehaviorSubject<number>(null);

  showSensorOnMap($event: SensorInterface) {
    this.sensorOnMap.next($event);
  }

  alterActiveArea($event: number) {
    this.tableHeight.next($event);
  }
}
