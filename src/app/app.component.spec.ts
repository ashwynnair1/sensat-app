import { TestBed, async } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { AppComponent } from './app.component';
import { Component, EventEmitter, Input, Output } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { SensorInterface } from './core/models/sensor.interface';
import { By } from '@angular/platform-browser';

describe('AppComponent', () => {

  const fakeSensor: SensorInterface = {
    id: 'fake-id',
    box_id: 'fake-box_id',
    sensor_type: 'fake-sensor_type',
    unit: 'fake-unit',
    name: 'fake-name',
    range_l: 1,
    range_u: 1,
    longitude: 1,
    latitude: 1,
    reading: 1,
    reading_ts: 'fake-reading_ts'
  };
  const fakeTableHeight = 10;

  @Component({
    selector: 'app-map',
    template: '<div></div>'
  })
  class FakeMapComponent {
    @Input() sensorOnMap: BehaviorSubject<SensorInterface>;
    @Input() tableHeight: BehaviorSubject<number> = new BehaviorSubject<number>(null);
    @Output() closeMap = new EventEmitter();
  }

  @Component({
    selector: 'app-table',
    template: '<div></div>'
  })
  class FakeTableComponent {
    @Input() sensorOnMap: BehaviorSubject<SensorInterface> = new BehaviorSubject<SensorInterface>(null);
    @Output() showMap = new EventEmitter<SensorInterface>();
    @Output() setActiveAreaHeight = new EventEmitter<number>();
  }

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule
      ],
      declarations: [
        AppComponent,
        FakeMapComponent,
        FakeTableComponent
      ],
      schemas: [ ]
    }).compileComponents();
  }));

  it('should create the app', () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.componentInstance;
    expect(app).toBeTruthy();
  });

  it('should contain app-map component', () => {
    const fixture = TestBed.createComponent(AppComponent);
    const mapEl = fixture.debugElement.query(By.directive(FakeMapComponent));
    expect(mapEl).toBeTruthy();
  });

  it('should contain app-table component', () => {
    const fixture = TestBed.createComponent(AppComponent);
    const tableEl = fixture.debugElement.query(By.directive(FakeTableComponent));
    expect(tableEl).toBeTruthy();
  });

  it('should pass down fakeSensor within sensorOnMap to app-map component', () => {
    const fixture = TestBed.createComponent(AppComponent);
    const mapEl = fixture.debugElement.query(By.directive(FakeMapComponent));
    const mapComp = mapEl.injector.get(FakeMapComponent) as FakeMapComponent;
    fixture.componentInstance.sensorOnMap.next(fakeSensor);
    fixture.detectChanges();
    expect(mapComp.sensorOnMap.getValue()).toEqual(fakeSensor);
  });

  it('should not pass down fakeSensor within sensorOnMap to app-map component', () => {
    const fixture = TestBed.createComponent(AppComponent);
    const mapEl = fixture.debugElement.query(By.directive(FakeMapComponent));
    const mapComp = mapEl.injector.get(FakeMapComponent) as FakeMapComponent;
    fixture.detectChanges();
    expect(mapComp.sensorOnMap.getValue()).not.toEqual(fakeSensor);
  });

  it('should pass down fakeSensor within sensorOnMap to app-table component', () => {
    const fixture = TestBed.createComponent(AppComponent);
    const tableEl = fixture.debugElement.query(By.directive(FakeTableComponent));
    const tableComp = tableEl.injector.get(FakeTableComponent) as FakeTableComponent;
    fixture.componentInstance.sensorOnMap.next(fakeSensor);
    fixture.detectChanges();
    expect(tableComp.sensorOnMap.getValue()).toEqual(fakeSensor);
  });

  it('should not pass down fakeSensor within sensorOnMap to app-table component', () => {
    const fixture = TestBed.createComponent(AppComponent);
    const tableEl = fixture.debugElement.query(By.directive(FakeTableComponent));
    const tableComp = tableEl.injector.get(FakeTableComponent) as FakeTableComponent;
    fixture.detectChanges();
    expect(tableComp.sensorOnMap.getValue()).not.toEqual(fakeSensor);
  });

  it('should pass down new value for tableHeight to app-map component', () => {
    const fixture = TestBed.createComponent(AppComponent);
    const mapEl = fixture.debugElement.query(By.directive(FakeMapComponent));
    const mapComp = mapEl.injector.get(FakeMapComponent) as FakeMapComponent;
    fixture.componentInstance.tableHeight.next(fakeTableHeight);
    fixture.detectChanges();
    expect(mapComp.tableHeight.getValue()).toEqual(fakeTableHeight);
  });

  it('should not pass down new value for tableHeight to app-map component', () => {
    const fixture = TestBed.createComponent(AppComponent);
    const mapEl = fixture.debugElement.query(By.directive(FakeMapComponent));
    const mapComp = mapEl.injector.get(FakeMapComponent) as FakeMapComponent;
    fixture.detectChanges();
    expect(mapComp.tableHeight.getValue()).not.toEqual(fakeTableHeight);
  });

  it('should updated sensorOnMap when closemap event is called', () => {
    const fixture = TestBed.createComponent(AppComponent);
    fixture.componentInstance.sensorOnMap.next(fakeSensor);
    fixture.detectChanges();
    const mapEl = fixture.debugElement.query(By.directive(FakeMapComponent));
    const mapComp = mapEl.injector.get(FakeMapComponent) as FakeMapComponent;
    mapComp.closeMap.emit();
    fixture.detectChanges();
    expect(fixture.componentInstance.sensorOnMap.getValue()).toBeNull();
  });

  it('should update tableHeight when setActiveAreaHeight event is called', () => {
    const fixture = TestBed.createComponent(AppComponent);
    const tableEl = fixture.debugElement.query(By.directive(FakeTableComponent));
    const tableComp = tableEl.injector.get(FakeTableComponent) as FakeTableComponent;
    tableComp.setActiveAreaHeight.emit(fakeTableHeight);
    fixture.detectChanges();
    expect(fixture.componentInstance.tableHeight.getValue()).toEqual(fakeTableHeight);
  });

  it('should update sensorOnMap when showMap event is called', () => {
    const fixture = TestBed.createComponent(AppComponent);
    const tableEl = fixture.debugElement.query(By.directive(FakeTableComponent));
    const tableComp = tableEl.injector.get(FakeTableComponent) as FakeTableComponent;
    tableComp.showMap.emit(fakeSensor);
    fixture.detectChanges();
    expect(tableComp.sensorOnMap.getValue()).toEqual(fakeSensor);
  });
});
