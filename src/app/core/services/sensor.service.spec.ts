import { TestBed } from '@angular/core/testing';
import { SensorService } from './sensor.service';
import { SensorInterface } from '../models/sensor.interface';

describe('SensorService', () => {
  let service: SensorService;
  const fakeTime1 = '2019-09-10T00:00:00';
  const fakeSensor: SensorInterface = {
    id: 'fake-id-1',
    box_id: 'fake-box_id-1',
    sensor_type: 'fake-sensor_type-1',
    unit: 'fake-unit-1',
    name: 'fake-name-1',
    range_l: 1,
    range_u: 1,
    longitude: 1,
    latitude: 1,
    reading: 1,
    reading_ts: fakeTime1
  };
  const fakeSensor1 = {
    invalid_key: 'fake-sensor_type-1',
  };

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(SensorService);
  });

  it('service should be created', () => {
    expect(service).toBeTruthy();
  });

  it('sensors by default should be observable of empty array', () => {
    expect(service.sensors.getValue()).toEqual([]);
  });

  it('sensor count should be greater than 0 when adding valid sensor', () => {
    service.addValidSensor(fakeSensor);
    expect(service.sensors.getValue().length).toBeGreaterThan(0);
  });

  it('sensor count should be greater than 0 when adding invalid sensor or other types', () => {
    service.addValidSensor(fakeSensor1);
    service.addValidSensor('fakeString');
    service.addValidSensor(123);
    expect(service.sensors.getValue().length).not.toBeGreaterThan(0);
  });

  it('should add sensors after fetch with oboe',  async () => {
    await service.fetchSensors();
    expect(service.sensors.getValue().length).toBeGreaterThan(0);
    expect(service.isStreamingSensors.getValue()).not.toBeTruthy();
  });

  it('getSensors calls fetchSensors and returns observable', () => {
    const spy = spyOn(service, 'fetchSensors').and.callThrough();
    const value = service.getSensors();
    expect(spy).toHaveBeenCalled();
  });
});
