import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { isSensor, SensorInterface } from '../models/sensor.interface';
import * as oboe from 'oboe';
import { TableFilterOptionsInterface } from '../models/table-filter-value.interface';
import { SENSOR_DATA_URL } from '../urls/urls';

@Injectable({
  providedIn: 'root'
})
export class SensorService {
  sensors: BehaviorSubject<SensorInterface[]> = new BehaviorSubject<SensorInterface[]>([]);
  isStreamingSensors: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
  sensorFilterOptions: BehaviorSubject<TableFilterOptionsInterface> = new BehaviorSubject<TableFilterOptionsInterface>({});
  sensorCount = 0;

  constructor() { }

  getSensors(): Observable<SensorInterface[]> {
    this.fetchSensors();
    return this.sensors;
  }

  updateFilterFormOptions(sensor: SensorInterface) {
    const sensorFilterOptions = this.sensorFilterOptions.getValue();
    if (!sensorFilterOptions[sensor.sensor_type]) {
      sensorFilterOptions[sensor.sensor_type] = sensor.name;
    }
    this.sensorFilterOptions.next(sensorFilterOptions);
  }

  fetchSensors() {
    return new Promise((resolve, reject) => {
      oboe(SENSOR_DATA_URL)
        .node('!', (res) => {
          this.isStreamingSensors.next(true);
          this.addValidSensor(res);
        })
        .on('end', () => {
          this.isStreamingSensors.next(false);
          resolve();
        })
        .on('fail', (err) => {
          this.isStreamingSensors.next(false);
          console.log(err);
          reject();
        });
    });
  }

  addValidSensor(sensor: any) {
    if (isSensor(sensor)) {
      this.sensors.next([...this.sensors.getValue(), sensor]);
      this.sensorCount++;
      this.updateFilterFormOptions(sensor);
    } else {
      console.log(sensor);
    }
  }
}
