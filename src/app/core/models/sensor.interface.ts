export interface SensorInterface {
  id: string;
  box_id: string;
  sensor_type: string;
  name: string;
  range_l: number;
  range_u: number;
  longitude: number;
  latitude: number;
  reading: number;
  unit: string;
  reading_ts: string;
}

export function isSensor(arg: any): arg is SensorInterface {
    return arg &&
      typeof(arg.id) === 'string' &&
      typeof(arg.box_id) ===  'string' &&
      typeof(arg.sensor_type) ===  'string' &&
      typeof(arg.name) ===  'string' &&
      typeof(arg.range_l) ===  'number' &&
      typeof(arg.range_u) ===  'number' &&
      typeof(arg.longitude) ===  'number' &&
      typeof(arg.latitude) ===  'number' &&
      typeof(arg.reading) ===  'number' &&
      typeof(arg.unit) ===  'string' &&
      typeof(arg.reading_ts) === 'string';
}
