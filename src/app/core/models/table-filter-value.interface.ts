export interface TableFilterOptionsInterface {
  [sensorType: string]: string; // relevant sensor name for sensor_type should be value
}
