import { AfterViewInit, Component, EventEmitter, Input, OnDestroy, Output } from '@angular/core';
import * as L from 'leaflet';
import { featureGroup, circleMarker } from 'leaflet';
import { SensorInterface } from '../../core/models/sensor.interface';
import { BehaviorSubject, combineLatest, Subscription } from 'rxjs';
import {
  bounceInDownOnEnterAnimation,
  bounceOutUpOnLeaveAnimation,
  pulseAnimation
} from 'angular-animations';
import { animate, state, style, transition, trigger } from '@angular/animations';

@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.scss'],
  animations: [
    bounceInDownOnEnterAnimation({ delay: 200 }),
    bounceOutUpOnLeaveAnimation(),
    pulseAnimation({ delay: 200 }),
    trigger('mapTable', [
      state('map', style({ opacity: 100 })),
      state('table', style({ opacity: 0 })),
      transition('table => map', [ animate('0.2s') ]),
      transition('map => table', [ animate('0.2s 1s') ]),
    ]),
  ]
})
export class MapComponent implements AfterViewInit, OnDestroy {

  @Input() sensorOnMap: BehaviorSubject<SensorInterface> = new BehaviorSubject<SensorInterface>(null);
  @Input() tableHeight: BehaviorSubject<number> = new BehaviorSubject<number>(null);
  @Output() closeMap = new EventEmitter();

  map: L.Map;
  sensorLayer = featureGroup();
  private subscription: Subscription = new Subscription();

  constructor() { }

  ngAfterViewInit(): void {
    const sensorOnMapSubscription = combineLatest([
      this.sensorOnMap,
      this.tableHeight,
    ]).subscribe(([sensor, height]) => {
      if (sensor) {
        if (!this.map) {
          this.initMap();
        }
        if (this.map) {
          this.generateMarker(sensor.latitude, sensor.longitude);
          this.panToSensor(sensor);
        }
      }
    });
    this.subscription.add(sensorOnMapSubscription);
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

  initMap(): void {
    this.map = L.map('map', {
      zoom: 8,
    });

    const baseMap = L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
      maxZoom: 19,
      attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>'
    });
    this.map.addLayer(baseMap);
    this.map.addLayer(this.sensorLayer);
  }

  generateMarker(lat: number, lng: number): void {
    this.sensorLayer.clearLayers();
    const marker = circleMarker([lat, lng], {
      color: '#000000',
      fillColor: '#FF2F35',
      fillOpacity: 1,
      radius: 6,
      weight: 1.5,
    });
    this.sensorLayer.addLayer(marker);
  }

  panToSensor(sensor: SensorInterface): void {
    const sensorPixels = this.map.project({ lat: sensor.latitude, lng: sensor.longitude }, this.map.getZoom());
    const offset = this.getOffset();
    const latLngWithOffset = this.map.unproject([sensorPixels.x, sensorPixels.y + offset], this.map.getZoom());
    this.map.panTo(latLngWithOffset);
  }

  getOffset(): number {
    const mapHeight = this.map.getSize().y;
    const centreOfMap = mapHeight / 2;
    const upperBound = 75;
    const lowerBound = this.tableHeight.getValue();
    const centreOfActiveArea = (mapHeight + upperBound - lowerBound) / 2;
    return centreOfMap - centreOfActiveArea;
  }

  returnToTableView() {
    this.closeMap.emit();
  }
}
