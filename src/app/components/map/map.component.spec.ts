import {async, ComponentFixture, fakeAsync, TestBed, tick} from '@angular/core/testing';

import { MapComponent } from './map.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { SensorInterface } from '../../core/models/sensor.interface';
import { MatIconModule } from '@angular/material/icon';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatButtonModule } from '@angular/material/button';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

describe('MapComponent', () => {
  let component: MapComponent;
  let fixture: ComponentFixture<MapComponent>;
  const fakeTime1 = '2019-09-10T00:00:00';
  const fakeSensor: SensorInterface = {
    id: 'fake-id-1',
    box_id: 'fake-box_id-1',
    sensor_type: 'fake-sensor_type-1',
    unit: 'fake-unit-1',
    name: 'fake-name-1',
    range_l: 1,
    range_u: 1,
    longitude: -0.06507,
    latitude: 51.51885,
    reading: 1,
    reading_ts: fakeTime1
  };

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MapComponent ],
      imports: [
        BrowserAnimationsModule,
        MatIconModule,
        MatTooltipModule,
        MatButtonModule,
      ],
      schemas: [ CUSTOM_ELEMENTS_SCHEMA ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    jasmine.DEFAULT_TIMEOUT_INTERVAL = 20000;
    fixture = TestBed.createComponent(MapComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should initialise map when sensorOnMap is updated to fakeSensor', async(() => {
    const spy = spyOn(component, 'initMap');
    expect(spy).not.toHaveBeenCalled();
    component.sensorOnMap.next(fakeSensor);
    fixture.detectChanges();
    expect(spy).toHaveBeenCalled();
  }));

  it('should generate layer on sensorLayer when sensorOnMap is updated to fakeSensor', async(() => {
    const spy = spyOn(component, 'generateMarker');
    expect(spy).not.toHaveBeenCalled();
    component.sensorOnMap.next(fakeSensor);
    fixture.detectChanges();
    expect(spy).toHaveBeenCalled();
  }));

  it('should add a layer to sensorLayer from fakeSensor', async(() => {
    component.sensorOnMap.next(fakeSensor);
    fixture.autoDetectChanges();
    expect(component.sensorLayer.getLayers().length).toBeGreaterThan(0);
  }));

  it('should pantoSensor for fakeSensor', async(() => {
    const spy = spyOn(component, 'panToSensor');
    expect(spy).not.toHaveBeenCalled();
    component.sensorOnMap.next(fakeSensor);
    fixture.detectChanges();
    expect(spy).toHaveBeenCalled();
  }));

  it('should emit closeMap when close button is clicked', async(() => {
    const spy = spyOn(component.closeMap, 'emit');
    expect(spy).not.toHaveBeenCalled();
    component.sensorOnMap.next(fakeSensor);
    fixture.detectChanges();
    const button = fixture.debugElement.nativeElement.querySelector('#close-button');
    button.click();
    expect(spy).toHaveBeenCalled();
  }));
});
