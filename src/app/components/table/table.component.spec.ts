import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { TableComponent } from './table.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { TranslateLoader, TranslateModule} from '@ngx-translate/core';
import { HttpClient, HttpClientModule} from '@angular/common/http';
import { HttpLoaderFactory } from '../../app.module';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatSelectModule } from '@angular/material/select';
import { Component, EventEmitter, Input, Output } from '@angular/core';
import { SensorInterface } from '../../core/models/sensor.interface';
import { TableFilterOptionsInterface } from '../../core/models/table-filter-value.interface';
import { SensorService } from '../../core/services/sensor.service';
import { cold , getTestScheduler } from 'jasmine-marbles';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatTableModule } from '@angular/material/table';
import { MatButtonModule } from '@angular/material/button';
import { MatSortModule, Sort } from '@angular/material/sort';
import { BrowserModule, By } from '@angular/platform-browser';
import { AppRoutingModule } from '../../app-routing.module';
import { ReactiveFormsModule } from '@angular/forms';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatIconModule } from '@angular/material/icon';
import { AngularResizedEventModule } from 'angular-resize-event';
import { SensorColumnsEnum } from './enum/sensor.enum';

describe('TableComponent', () => {
  let component: TableComponent;
  let fixture: ComponentFixture<TableComponent>;
  let sensorService: any;
  const fakeTime1 = '2019-09-10T00:00:00';
  const fakeTime2 = '2019-09-10T00:30:00';
  const fakeFilterOptions: TableFilterOptionsInterface = {
    'fake-sensor_type-1': 'fake-name-1',
    'fake-sensor_type-2': 'fake-name-2',
  };
  const fakeFilterChoice: string[] = ['fake-sensor_type-1'];
  const fakeSort: Sort = { active: SensorColumnsEnum.SENSOR_TYPE.toString(), direction: 'desc' };
  const fakeSensor: SensorInterface = {
    id: 'fake-id-1',
    box_id: 'fake-box_id-1',
    sensor_type: 'fake-sensor_type-1',
    unit: 'fake-unit-1',
    name: 'fake-name-1',
    range_l: 1,
    range_u: 1,
    longitude: 1,
    latitude: 1,
    reading: 1,
    reading_ts: fakeTime1
  };
  const fakeSensorMarbles = '-a---b-c';
  const fakeSensorMarblesValues: { [marble: string]: SensorInterface[] } = {
    a: [],
    b: [
      {
        id: 'fake-id-1',
        box_id: 'fake-box_id-1',
        sensor_type: 'fake-sensor_type-1',
        unit: 'fake-unit-1',
        name: 'fake-name-1',
        range_l: 1,
        range_u: 1,
        longitude: 1,
        latitude: 1,
        reading: 1,
        reading_ts: fakeTime1
      },
      {
        id: 'fake-id-2',
        box_id: 'fake-box_id-2',
        sensor_type: 'fake-sensor_type-2',
        unit: 'fake-unit-2',
        name: 'fake-name-2',
        range_l: 2,
        range_u: 2,
        longitude: 2,
        latitude: 2,
        reading: 2,
        reading_ts: fakeTime1
      },
      {
        id: 'fake-id-3',
        box_id: 'fake-box_id-3',
        sensor_type: 'fake-sensor_type-3',
        unit: 'fake-unit-3',
        name: 'fake-name-3',
        range_l: 3,
        range_u: 3,
        longitude: 3,
        latitude: 3,
        reading: 3,
        reading_ts: fakeTime1
      }, {
        id: 'fake-id-4',
        box_id: 'fake-box_id-4',
        sensor_type: 'fake-sensor_type-4',
        unit: 'fake-unit-4',
        name: 'fake-name-4',
        range_l: 4,
        range_u: 4,
        longitude: 4,
        latitude: 4,
        reading: 4,
        reading_ts: fakeTime1
      }, {
        id: 'fake-id-5',
        box_id: 'fake-box_id-5',
        sensor_type: 'fake-sensor_type-5',
        unit: 'fake-unit-5',
        name: 'fake-name-5',
        range_l: 5,
        range_u: 5,
        longitude: 5,
        latitude: 5,
        reading: 5,
        reading_ts: fakeTime1
      }, {
        id: 'fake-id-6',
        box_id: 'fake-box_id-6',
        sensor_type: 'fake-sensor_type-6',
        unit: 'fake-unit-6',
        name: 'fake-name-6',
        range_l: 6,
        range_u: 6,
        longitude: 6,
        latitude: 6,
        reading: 6,
        reading_ts: fakeTime1
      }],
    c: [
      {
        id: 'fake-id-1',
        box_id: 'fake-box_id-1',
        sensor_type: 'fake-sensor_type-1',
        unit: 'fake-unit-1',
        name: 'fake-name-1',
        range_l: 1,
        range_u: 1,
        longitude: 1,
        latitude: 1,
        reading: 1,
        reading_ts: fakeTime1
      },
      {
        id: 'fake-id-2',
        box_id: 'fake-box_id-2',
        sensor_type: 'fake-sensor_type-2',
        unit: 'fake-unit-2',
        name: 'fake-name-2',
        range_l: 2,
        range_u: 2,
        longitude: 2,
        latitude: 2,
        reading: 2,
        reading_ts: fakeTime1
      },
      {
        id: 'fake-id-3',
        box_id: 'fake-box_id-3',
        sensor_type: 'fake-sensor_type-3',
        unit: 'fake-unit-3',
        name: 'fake-name-3',
        range_l: 3,
        range_u: 3,
        longitude: 3,
        latitude: 3,
        reading: 3,
        reading_ts: fakeTime1
      }, {
        id: 'fake-id-4',
        box_id: 'fake-box_id-4',
        sensor_type: 'fake-sensor_type-4',
        unit: 'fake-unit-4',
        name: 'fake-name-4',
        range_l: 4,
        range_u: 4,
        longitude: 4,
        latitude: 4,
        reading: 4,
        reading_ts: fakeTime1
      }, {
        id: 'fake-id-5',
        box_id: 'fake-box_id-5',
        sensor_type: 'fake-sensor_type-5',
        unit: 'fake-unit-5',
        name: 'fake-name-5',
        range_l: 5,
        range_u: 5,
        longitude: 5,
        latitude: 5,
        reading: 5,
        reading_ts: fakeTime1
      }, {
        id: 'fake-id-6',
        box_id: 'fake-box_id-6',
        sensor_type: 'fake-sensor_type-6',
        unit: 'fake-unit-6',
        name: 'fake-name-6',
        range_l: 6,
        range_u: 6,
        longitude: 6,
        latitude: 6,
        reading: 6,
        reading_ts: fakeTime1
      }, {
        id: 'fake-id-7',
        box_id: 'fake-box_id-7',
        sensor_type: 'fake-sensor_type-7',
        unit: 'fake-unit-7',
        name: 'fake-name-7',
        range_l: 7,
        range_u: 7,
        longitude: 7,
        latitude: 7,
        reading: 7,
        reading_ts: fakeTime1
      }, {
        id: 'fake-id-8',
        box_id: 'fake-box_id-8',
        sensor_type: 'fake-sensor_type-8',
        unit: 'fake-unit-8',
        name: 'fake-name-8',
        range_l: 8,
        range_u: 8,
        longitude: 8,
        latitude: 8,
        reading: 8,
        reading_ts: fakeTime1
      }, {
        id: 'fake-id-9',
        box_id: 'fake-box_id-9',
        sensor_type: 'fake-sensor_type-9',
        unit: 'fake-unit-9',
        name: 'fake-name-9',
        range_l: 9,
        range_u: 9,
        longitude: 9,
        latitude: 9,
        reading: 9,
        reading_ts: fakeTime1
      }, {
        id: 'fake-id-10',
        box_id: 'fake-box_id-10',
        sensor_type: 'fake-sensor_type-10',
        unit: 'fake-unit-10',
        name: 'fake-name-10',
        range_l: 10,
        range_u: 10,
        longitude: 10,
        latitude: 10,
        reading: 10,
        reading_ts: fakeTime1
      }, {
        id: 'fake-id-11',
        box_id: 'fake-box_id-11',
        sensor_type: 'fake-sensor_type-11',
        unit: 'fake-unit-11',
        name: 'fake-name-11',
        range_l: 11,
        range_u: 11,
        longitude: 11,
        latitude: 11,
        reading: 11,
        reading_ts: fakeTime1
      }, {
        id: 'fake-id-12',
        box_id: 'fake-box_id-12',
        sensor_type: 'fake-sensor_type-12',
        unit: 'fake-unit-12',
        name: 'fake-name-12',
        range_l: 12,
        range_u: 12,
        longitude: 12,
        latitude: 12,
        reading: 12,
        reading_ts: fakeTime1
      }, {
        id: 'fake-id-13',
        box_id: 'fake-box_id-13',
        sensor_type: 'fake-sensor_type-13',
        unit: 'fake-unit-13',
        name: 'fake-name-13',
        range_l: 13,
        range_u: 113,
        longitude: 13,
        latitude: 13,
        reading: 13,
        reading_ts: fakeTime2
      }, {
        id: 'fake-id-14',
        box_id: 'fake-box_id-14',
        sensor_type: 'fake-sensor_type-14',
        unit: 'fake-unit-14',
        name: 'fake-name-14',
        range_l: 14,
        range_u: 14,
        longitude: 14,
        latitude: 14,
        reading: 14,
        reading_ts: fakeTime2
      }, {
        id: 'fake-id-15',
        box_id: 'fake-box_id-15',
        sensor_type: 'fake-sensor_type-15',
        unit: 'fake-unit-15',
        name: 'fake-name-15',
        range_l: 15,
        range_u: 15,
        longitude: 15,
        latitude: 15,
        reading: 15,
        reading_ts: fakeTime2
      }, {
        id: 'fake-id-16',
        box_id: 'fake-box_id-16',
        sensor_type: 'fake-sensor_type-16',
        unit: 'fake-unit-16',
        name: 'fake-name-16',
        range_l: 16,
        range_u: 16,
        longitude: 16,
        latitude: 16,
        reading: 16,
        reading_ts: fakeTime2
      }, {
        id: 'fake-id-17',
        box_id: 'fake-box_id-17',
        sensor_type: 'fake-sensor_type-17',
        unit: 'fake-unit-17',
        name: 'fake-name-17',
        range_l: 17,
        range_u: 17,
        longitude: 17,
        latitude: 17,
        reading: 17,
        reading_ts: fakeTime2
      }, {
        id: 'fake-id-18',
        box_id: 'fake-box_id-18',
        sensor_type: 'fake-sensor_type-18',
        unit: 'fake-unit-18',
        name: 'fake-name-18',
        range_l: 18,
        range_u: 18,
        longitude: 18,
        latitude: 18,
        reading: 18,
        reading_ts: fakeTime2
      }, {
        id: 'fake-id-19',
        box_id: 'fake-box_id-19',
        sensor_type: 'fake-sensor_type-19',
        unit: 'fake-unit-19',
        name: 'fake-name-19',
        range_l: 19,
        range_u: 19,
        longitude: 19,
        latitude: 19,
        reading: 19,
        reading_ts: fakeTime2
      }, {
        id: 'fake-id-20',
        box_id: 'fake-box_id-20',
        sensor_type: 'fake-sensor_type-20',
        unit: 'fake-unit-20',
        name: 'fake-name-20',
        range_l: 20,
        range_u: 20,
        longitude: 20,
        latitude: 20,
        reading: 20,
        reading_ts: fakeTime2
      },
      {
        id: 'fake-id-21',
        box_id: 'fake-box_id-21',
        sensor_type: 'fake-sensor_type-21',
        unit: 'fake-unit-21',
        name: 'fake-name-21',
        range_l: 21,
        range_u: 21,
        longitude: 21,
        latitude: 21,
        reading: 21,
        reading_ts: fakeTime2
      },
      {
        id: 'fake-id-22',
        box_id: 'fake-box_id-22',
        sensor_type: 'fake-sensor_type-22',
        unit: 'fake-unit-22',
        name: 'fake-name-22',
        range_l: 22,
        range_u: 22,
        longitude: 22,
        latitude: 22,
        reading: 22,
        reading_ts: fakeTime2
      }
    ],
  };

  @Component({
    selector: 'app-table-filter',
    template: '<div></div>'
  })
  class FakeTableFilterComponent {
    @Output() filterFormValue = new EventEmitter<string[]>();
  }

  beforeEach(async(() => {
    sensorService = jasmine.createSpy('SensorService');
    sensorService.getSensors = () => cold(fakeSensorMarbles, fakeSensorMarblesValues);
    TestBed.configureTestingModule({
      declarations: [ TableComponent, FakeTableFilterComponent ],
      imports: [
        BrowserModule,
        AppRoutingModule,
        BrowserAnimationsModule,
        MatTableModule,
        MatPaginatorModule,
        MatSortModule,
        HttpClientModule,
        TranslateModule.forRoot({
          loader: {
            provide: TranslateLoader,
            useFactory: HttpLoaderFactory,
            deps: [HttpClient]
          }
        }),
        ReactiveFormsModule,
        MatFormFieldModule,
        MatSelectModule,
        MatTooltipModule,
        MatButtonModule,
        MatIconModule,
        MatSnackBarModule,
        AngularResizedEventModule
      ],
      providers: [{ provide: SensorService, useValue: sensorService }],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should update sensors using marbles', async(() => {
    getTestScheduler().flush();
    fixture.autoDetectChanges();
    expect(fixture.componentInstance.sensors).toEqual(fakeSensorMarblesValues.c);
  }));

  it('should display correct number of rows of sensors based on pagesize', async(() => {
    getTestScheduler().flush();
    fixture.autoDetectChanges();
    const pageSize = component.paginator.pageSize;
    const tableRows = fixture.nativeElement.querySelectorAll('tr');
    expect(tableRows.length).toEqual(pageSize + 1); // include header row
  }));

  it('should not display same number of rows after page change', async(() => {
    getTestScheduler().flush();
    fixture.autoDetectChanges();
    const pageSize = component.paginator.pageSize;
    if (component.paginator._displayedPageSizeOptions.length > 1) {
      let newPageSize: number;
      component.paginator._displayedPageSizeOptions.forEach(size => {
        if (size !== pageSize) {
          newPageSize = size;
        }
      });
      if (newPageSize) {
        component.paginator._changePageSize(newPageSize);
        const tableRows = fixture.nativeElement.querySelectorAll('tr');
        expect(tableRows.length).not.toEqual(pageSize + 1); // include header row
      }
    }
  }));

  it('should only have two columns to sort (sensor_type and reading_ts - tested against aria-labels)', async(() => {
    getTestScheduler().flush();
    fixture.autoDetectChanges();
    const elements = fixture.nativeElement.querySelectorAll('button.mat-sort-header-button');
    const elAriaLabels = [];
    elements.forEach(el => elAriaLabels.push(el.getAttribute('aria-label')));
    expect(elements.length).toEqual(2);
    expect(elAriaLabels.every(label =>
      [SensorColumnsEnum.SENSOR_TYPE, SensorColumnsEnum.READING_TS]
        .map(name => 'Change sorting for ' + name)
        .includes(label.toString())))
      .toBeTruthy();
  }));

  it('should correctly call method to update currentSort when button is clicked', async(() => {
    getTestScheduler().flush();
    fixture.autoDetectChanges();
    const spy = spyOn(component, 'sortTable').and.callThrough();
    expect(spy).not.toHaveBeenCalled();
    expect(!!component.currentSort.active && !!component.currentSort.direction).not.toBeTruthy();
    const buttons = fixture.nativeElement.querySelectorAll('button.mat-sort-header-button');
    buttons[0].click();
    expect(spy).toHaveBeenCalled();
    expect(!!component.currentSort.active && !!component.currentSort.direction).toBeTruthy();
  }));

  it('should correctly sort sensors when sortSensors method is called', async(() => {
    getTestScheduler().flush();
    fixture.autoDetectChanges();
    component.sortTable(fakeSort);
    expect(component.sortedSensors.every((sensor, index) => {
      if (index > 0) {
        return sensor.sensor_type <= component.sortedSensors[index - 1].sensor_type;
      }
      return true;
    })).toBeTruthy();
  }));

  it('should filter sortedSensors when event emitted from to app-table-filter component', () => {
    const filterEL = fixture.debugElement.query(By.directive(FakeTableFilterComponent));
    const filterComp = filterEL.injector.get(FakeTableFilterComponent) as FakeTableFilterComponent;
    filterComp.filterFormValue.emit(fakeFilterChoice);
    fixture.detectChanges();
    expect(component.sortedSensors.every(sensor => fakeFilterChoice.includes(sensor.sensor_type))).toBeTruthy();
  });

  it('should emit showMap event when view button is clicked', () => {
    getTestScheduler().flush();
    fixture.autoDetectChanges();
    const buttons = fixture.nativeElement.querySelectorAll('button.mat-flat-button');
    const spy = spyOn(component.showMap, 'emit');
    expect(spy).not.toHaveBeenCalled();
    buttons[0].click();
    expect(spy).toHaveBeenCalled();
  });

  it('should reset reduce pagination when sensorOnMap is fakeSensor', () => {
    getTestScheduler().flush();
    fixture.autoDetectChanges();
    component.sensorOnMap.next(fakeSensor);
    expect(component.paginator.pageSize).toEqual(3);
  });

  it('should only have sensors with same box as fakeSensor in sortedSensors when it is sensorOnMap', () => {
    getTestScheduler().flush();
    fixture.autoDetectChanges();
    component.sensorOnMap.next(fakeSensor);
    expect(component.sortedSensors.every(sensor => sensor.box_id === fakeSensor.box_id)).toBeTruthy();
  });
});
