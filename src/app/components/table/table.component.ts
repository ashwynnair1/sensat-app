import {
  Component,
  EventEmitter,
  Input,
  OnDestroy,
  OnInit,
  Output,
  ViewChild
} from '@angular/core';
import { SensorService } from '../../core/services/sensor.service';
import { SensorInterface } from '../../core/models/sensor.interface';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { TranslateService } from '@ngx-translate/core';
import { Sort } from '@angular/material/sort';
import { SensorColumnsEnum } from './enum/sensor.enum';
import { BehaviorSubject, Subscription } from 'rxjs';
import { TableFilterOptionsInterface } from '../../core/models/table-filter-value.interface';
import { animate, state, style, transition, trigger } from '@angular/animations';
import { ResizedEvent } from 'angular-resize-event';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.scss'],
  animations: [
    trigger('mapTable', [
      state('map', style({
        bottom: '30px',
        transform: 'translateX(-50%) translateY(0)'
      })),
      state('table', style({
        bottom: '50%',
        transform: 'translateX(-50%) translateY(50%)'
      })),
      transition('table => map', [
        animate('0.2s')
      ]),
      transition('map => table', [
        animate('0.2s 1s')
      ]),
    ]),
  ],
})
export class TableComponent implements OnInit, OnDestroy {

  @Input() sensorOnMap: BehaviorSubject<SensorInterface> = new BehaviorSubject<SensorInterface>(null);
  @Output() showMap = new EventEmitter<SensorInterface>();
  @Output() setActiveAreaHeight = new EventEmitter<number>();

  constructor(
    private sensorService: SensorService,
    private translate: TranslateService,
    private snackbar: MatSnackBar
  ) {
    translate.setDefaultLang('user_translation');
  }

  sensorDataSource: MatTableDataSource<SensorInterface>;
  sortedSensors: SensorInterface[] = [];
  sensors: SensorInterface[] = [];
  currentSort: Sort = { active: null, direction: null };
  dataFetchInProgress: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
  currentFilter: string[] = [];
  allowColumnsToSort: string[] = [
    SensorColumnsEnum.SENSOR_TYPE.toString(),
    SensorColumnsEnum.READING_TS.toString(),
  ];
  readonly SensorColumnsEnum = SensorColumnsEnum;
  subscription: Subscription = new Subscription();

  tableViewPageSize = 5;
  @ViewChild(MatPaginator) paginator: MatPaginator;

  ngOnInit(): void {
    const getSensorsSubscription = this.sensorService.getSensors().subscribe((sensors: SensorInterface[]) => {
      this.sensors = sensors;
      this.updateDataSource();
    });
    const sensorOnMapSubscription = this.sensorOnMap.subscribe((sensor) => {
      this.resetPagination(sensor);
      this.toggleSnackbar(sensor);
      this.updateDataSource();
    });
    this.dataFetchInProgress = this.sensorService.isStreamingSensors;
    this.subscription.add(getSensorsSubscription);
    this.subscription.add(sensorOnMapSubscription);
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

  private storeTablePageSize() {
    this.tableViewPageSize = this.paginator.pageSize;
  }

  private reduceTableSize() {
    this.paginator._changePageSize(3);
    this.paginator.firstPage();
  }

  sortTable($event: Sort): void {
    this.currentSort = $event;
    this.updateDataSource();
  }

  filterTable($event: string[]) {
    this.currentFilter = $event;
    this.updateDataSource();
  }

  private resetPagination(sensor: SensorInterface) {
    if (this.paginator) {
      sensor ?
        this.reduceTableSize() :
        setTimeout( () => this.paginator._changePageSize(this.tableViewPageSize), 1000);
    }
  }

  private updateDataSource(): void {
    const col: string = this.currentSort.active;
    const direction: string = this.currentSort.direction;
    const sensorOnMap: SensorInterface = this.sensorOnMap.getValue();
    const filteredSensors: SensorInterface[] = this.currentFilter.length || sensorOnMap ?
      this.sensors.filter(sensor => {
        const matchesCurrentFilter: boolean = this.currentFilter.length ?
          this.currentFilter.includes(sensor.sensor_type) :
          true;
        const matchesCurrentSensorOnMap: boolean = sensorOnMap ? sensor.box_id === sensorOnMap.box_id : true;
        return matchesCurrentFilter && matchesCurrentSensorOnMap;
      }) :
      this.sensors;
    if (this.allowColumnsToSort.includes(col) && direction) {
      this.sortedSensors = [...filteredSensors].sort((a: SensorInterface, b: SensorInterface) => {
          const flipSort: number = direction === 'desc' ? -1 : 1;
          return a[col] < b[col] ? -1 * flipSort : a[col] > b[col] ? flipSort : 0;
        }
      );
    } else {
      this.sortedSensors = [...filteredSensors];
    }
    this.sensorDataSource = new MatTableDataSource<SensorInterface>(this.sortedSensors);
    this.sensorDataSource.paginator = this.paginator;
  }

  getSensorColumns(): string[] {
    return Object.values(SensorColumnsEnum);
  }

  showSensorOnMap(sensor: SensorInterface) {
    const loadingMap = !this.sensorOnMap.getValue();
    if (loadingMap) {
      this.storeTablePageSize();
    }
    this.showMap.emit(sensor);
  }

  onResize($event: ResizedEvent) {
    if ($event.oldHeight !== $event.newHeight) {
      this.setActiveAreaHeight.emit($event.newHeight);
    }
  }

  isWindowWideEnough() {
    return window.innerWidth > 500;
  }

  isWindowTallEnough() {
    return window.innerHeight > 600;
  }

  private toggleSnackbar(sensor: SensorInterface) {
    sensor ?
      this.snackbar.dismiss() :
      setTimeout( () =>
        this.snackbar.open('Showing results for all sensor boxes', 'Close', { duration: 2000 }), 1000);
  }
}
