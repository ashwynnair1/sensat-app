import { Component, OnInit, Input, Output, EventEmitter, OnDestroy } from '@angular/core';
import { FormControl } from '@angular/forms';
import { BehaviorSubject, Subscription } from 'rxjs';
import { TableFilterOptionsInterface } from '../../../core/models/table-filter-value.interface';
import { SensorService } from '../../../core/services/sensor.service';

@Component({
  selector: 'app-table-filter',
  templateUrl: './table-filter.component.html',
  styleUrls: ['./table-filter.component.scss']
})
export class TableFilterComponent implements OnInit, OnDestroy {

  @Output() filterFormValue = new EventEmitter<string[]>();

  filterFormOptions: BehaviorSubject<TableFilterOptionsInterface> = new BehaviorSubject<TableFilterOptionsInterface>({});
  subscription: Subscription = new Subscription();

  filterForm = new FormControl();

  constructor(private sensorService: SensorService) { }

  ngOnInit(): void {
    this.filterFormOptions = this.sensorService.sensorFilterOptions;
    const formSubscription = this.filterForm.valueChanges.subscribe(value => {
      this.filterFormValue.emit(value);
    });
    this.subscription.add(formSubscription);
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }
}
