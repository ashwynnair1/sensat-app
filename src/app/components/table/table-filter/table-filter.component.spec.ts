import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TableFilterComponent } from './table-filter.component';
import { TableFilterOptionsInterface } from '../../../core/models/table-filter-value.interface';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatSelectModule } from '@angular/material/select';
import { ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { By } from '@angular/platform-browser';
import { MatTooltipModule } from '@angular/material/tooltip';

describe('TableFilterComponent', () => {
  let component: TableFilterComponent;
  let fixture: ComponentFixture<TableFilterComponent>;
  const fakeFilterOptions: TableFilterOptionsInterface = {
    'fake-sensor_type-1': 'fake-name-1',
    'fake-sensor_type-2': 'fake-name-2',
  };

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TableFilterComponent ],
      imports: [
        BrowserAnimationsModule,
        MatFormFieldModule,
        MatSelectModule,
        ReactiveFormsModule,
        MatTooltipModule
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TableFilterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should provide options when filterFormValue is updated', async(() => {
    component.filterFormOptions.next(fakeFilterOptions);
    fixture.detectChanges();
    const debugElement = fixture.debugElement;
    const matSelect = debugElement.query(By.css('.mat-select-trigger')).nativeElement;
    matSelect.click();
    fixture.detectChanges();
    const matOptions = debugElement.queryAll(By.css('.mat-option'));
    expect(matOptions.length).toEqual(Object.values(fakeFilterOptions).length);
  }));

  it('should emit event when option is selected', async(() => {
    const spy = spyOn(component.filterFormValue, 'emit');
    component.filterFormOptions.next(fakeFilterOptions);
    fixture.detectChanges();
    expect(spy).not.toHaveBeenCalled();
    const debugElement = fixture.debugElement;
    const matSelect = debugElement.query(By.css('.mat-select-trigger')).nativeElement;
    matSelect.click();
    fixture.detectChanges();
    const matOption = debugElement.query(By.css('.mat-option')).nativeElement;
    matOption.click();
    fixture.detectChanges();
    fixture.whenStable().then( () => {
       expect(spy).toHaveBeenCalled();
    });
  }));
});
