export enum SensorColumnsEnum {
  ID = 'id',
  BOX_ID = 'box_id',
  SENSOR_TYPE = 'sensor_type',
  NAME = 'name',
  RANGE_L = 'range_l',
  RANGE_U = 'range_u',
  LONGITUDE = 'longitude',
  LATITUDE = 'latitude',
  READING = 'reading',
  UNIT = 'unit',
  READING_TS = 'reading_ts',
  VIEW_ON_MAP = 'view_on_map'
}
