import {browser, by, element, ElementArrayFinder, ElementFinder} from 'protractor';

export class AppPage {
  navigateTo(): Promise<unknown> {
    return browser.get(browser.baseUrl) as Promise<unknown>;
  }

  getTableRows(): ElementArrayFinder {
    return element.all(by.css('tr')) as ElementArrayFinder;
  }

  getFilter(): ElementFinder {
    return element(by.css('.mat-select#filter-form-drop-down')) as ElementFinder;
  }

  getSortableHeader(className: string): ElementFinder {
    return element(by.css('th.mat-sort-header.' + className)) as ElementFinder;
  }

  getSortableHeaders(): ElementArrayFinder {
    return element.all(by.css('th.mat-sort-header')) as ElementArrayFinder;
  }

  getOptionsOnPage(): ElementArrayFinder {
    return element.all(by.css('.mat-option')) as ElementArrayFinder;
  }

  getFilterOptions(): ElementArrayFinder {
    return element.all(by.css('.mat-option.filter-form-drop-down-option')) as ElementArrayFinder;
  }

  getColumnCells(className: string): ElementArrayFinder {
    return element.all(by.css('td.' + className)) as ElementArrayFinder;
  }

  getNextPageButton(): ElementFinder {
    return element(by.css('button.mat-paginator-navigation-next')) as ElementFinder;
  }

  getFirstPageButton(): ElementFinder {
    return element(by.css('button.mat-paginator-navigation-first')) as ElementFinder;
  }

  getPaginatorSizeSelector(): ElementFinder {
    return element(by.css('.mat-select#mat-select-1')) as ElementFinder;
  }

  getViewButtons(): ElementArrayFinder {
    return element.all(by.css('button.view-button')) as ElementArrayFinder;
  }

  getMapContainer(): ElementFinder {
    return element(by.css('.map-container')) as ElementFinder;
  }

  getCloseButton(): ElementFinder {
    return element(by.css('#close-button')) as ElementFinder;
  }

  getMarker(): ElementFinder {
    return element(by.css('path.leaflet-interactive')) as ElementFinder;
  }
}
