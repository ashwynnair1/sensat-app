import { AppPage } from './app.po';
import {browser, by, element, ElementArrayFinder, ElementFinder, logging, protractor} from 'protractor';
import { SensorColumnsEnum } from '../../src/app/components/table/enum/sensor.enum';

describe('Sensat App', () => {
  let page: AppPage;
  const allowedSortableHeaders = [SensorColumnsEnum.SENSOR_TYPE, SensorColumnsEnum.READING_TS];
  /* Set maxPageTurns to null to turn every page - may require changing jasmine.DEFAULT_TIMEOUT_INTERVAL
  this can be done by updating jasmineNodeOpts.defaultTimeoutInterval in ./e2e/protractor.conf.js */
  const maxPageTurns = 5;

  const checkRemainingPages  = async (callback: (array: any[]) => Promise<boolean>, parameterArray: any[], maxPages?: number) => {
    if (!(await callback(parameterArray))) {
      return false;
    }
    const nextPageButton = page.getNextPageButton();
    if (maxPages !== 0 && page && await nextPageButton.isEnabled()) {
      nextPageButton.click();
      return maxPages ?
        await checkRemainingPages(callback, parameterArray, maxPages - 1) :
        await checkRemainingPages(callback, parameterArray, maxPages);
    } else {
      return true;
    }
  };

  const isDataSorted = async (inputArray: any[]): Promise<boolean> => {
    const [className, orderBy] = inputArray;
    let dataSorted = true;
    await page.getColumnCells(className).then(async (cells: ElementFinder[]) => {
      let previousCellText: string;
      for (const cell of cells) {
        const cellText = await cell.getText();
        if (previousCellText) {
          if (orderBy === 'ascending') {
            if (previousCellText > cellText) {
              dataSorted = false;
            }
          } else if (orderBy === 'descending') {
            if (previousCellText < cellText) {
              dataSorted = false;
            }
          }
        }
        previousCellText = cellText;
      }
    });
    return dataSorted;
  };

  const isDataFiltered = async (inputArray: any[]): Promise<boolean> => {
    const [className, filterNames] = inputArray;
    let dataFiltered = true;
    await page.getColumnCells(className).then(async (cells: ElementFinder[]) => {
      for (const cell of cells) {
        const cellText = await cell.getText();
        if (!filterNames.includes(cellText)) {
          dataFiltered = false;
        }
      }
    });
    return dataFiltered;
  };

  const checkDataIsFilteredByColumn = (columnName: SensorColumnsEnum) => {
    const filter = page.getFilter();
    filter.click();
    browser.waitForAngular();
    const filterNames: string[] = [];
    page.getFilterOptions().then(async options => {
      for (const option of options) {
        if (Math.random() > 0.5) {
          option.click();
          const optionValue = await option.getAttribute('ng-reflect-value');
          filterNames.push(optionValue);
        }
      }
      browser.actions().sendKeys(protractor.Key.ESCAPE).perform();
      browser.waitForAngular();
      const columnClassName: string = 'mat-column-' + columnName.toString();
      const result = await checkRemainingPages(isDataFiltered, [columnClassName, filterNames], maxPageTurns );
      expect(result).toBeTruthy();
    });
  };

  const checkDataIsSortedByColumn = async (columnName: SensorColumnsEnum) => {
    page.getFirstPageButton().click();
    browser.waitForAngular();
    const columnClassName: string = 'mat-column-' + columnName.toString();
    const sortableHeader = page.getSortableHeader(columnClassName);
    browser.actions().mouseMove(sortableHeader).perform();
    browser.waitForAngular();
    sortableHeader.click();
    browser.waitForAngular();
    const orderBy = await sortableHeader.getAttribute('aria-sort');
    const result = await checkRemainingPages(isDataSorted, [columnClassName, orderBy], maxPageTurns );
    expect(result).toBeTruthy();
    if (orderBy === 'ascending') {
      await checkDataIsSortedByColumn(columnName);
    }
  };

  const setMaxPagination = () => {
    page.getPaginatorSizeSelector().click();
    browser.waitForAngular();
    page.getOptionsOnPage().then((options: ElementFinder[]) => {
      options[options.length - 1].click();
    });
    browser.waitForAngular();
    browser.actions().sendKeys(protractor.Key.ESCAPE).perform();
    browser.waitForAngular();
  };

  beforeEach(() => {
    page = new AppPage();
  });

  it('should display sensor data in table', () => {
    page.navigateTo();
    browser.waitForAngular();
    page.getTableRows().then((rows: ElementFinder[]) =>  {
      expect(rows.length).toEqual(6);
    });
  });

  describe('allows user to sort data by time and sensor type', () => {

    it('only headers user can sort by are time and sensor type', () => {
      page.navigateTo();
      browser.waitForAngular();
      page.getSortableHeaders().then((sortableHeaders: ElementFinder[]) => {
        sortableHeaders.forEach(sortableHeader => {
          sortableHeader.getAttribute('class').then(classNames => {
            expect(allowedSortableHeaders.some(allowedSortableHeader => classNames.includes(allowedSortableHeader))).toBeTruthy();
          });
        });
      });
    });

    it('data sorts correctly for sensor_type', () => {
      page.navigateTo();
      browser.waitForAngular();
      setMaxPagination();
      browser.waitForAngular();
      checkDataIsSortedByColumn(SensorColumnsEnum.SENSOR_TYPE);
    });

    it('data sorts correctly for reading_ts', () => {
      page.navigateTo();
      browser.waitForAngular();
      setMaxPagination();
      browser.waitForAngular();
      checkDataIsSortedByColumn(SensorColumnsEnum.READING_TS);
    });

  });

  it('allows the user to filter data by sensor type/name.', () => {
    page.navigateTo();
    browser.waitForAngular();
    setMaxPagination();
    browser.waitForAngular();
    checkDataIsFilteredByColumn(SensorColumnsEnum.SENSOR_TYPE);
  });

  it('allow user to see sensor location on a map', () => {
    page.navigateTo();
    browser.waitForAngular();
    page.getViewButtons().then((viewButtons: ElementFinder[]) => {
      const index = Math.floor(Math.random() * 5);
      viewButtons[index].click();
      browser.waitForAngular();
      const mapContainerEl = page.getMapContainer();
      mapContainerEl.getAttribute('style').then(async style => {
        let isMaxOpacity: boolean;
        let isMarkerPresent: boolean;
        isMaxOpacity = style.includes('opacity: 100;');
        isMarkerPresent = await page.getMarker().isPresent();
        expect(isMaxOpacity && isMarkerPresent).toBeTruthy();
        page.getCloseButton().click();
        browser.waitForAngular();
        mapContainerEl.getAttribute('style').then(async newStyle => {
          isMaxOpacity = newStyle.includes('opacity: 100;');
          isMarkerPresent = await page.getMarker().isPresent();
          expect(isMaxOpacity && isMarkerPresent).not.toBeTruthy();
        });
      });
    });
  });

  afterEach(async () => {
    // Assert that there are no errors emitted from the browser
    const logs = await browser.manage().logs().get(logging.Type.BROWSER);
    expect(logs).not.toContain(jasmine.objectContaining({
      level: logging.Level.SEVERE,
    } as logging.Entry));
  });
});
