# SensatApp

## Overview

### Context
Some environmental sensors have been deployed on the field. There are various types of sensors (CO, Temperature, O3, SO2, Humidity, …). Sensors are housed in boxes, and a box contains one sensor of each kind. Boxes have been placed at various locations. The sensor data has been collected in a JSON file with the following schema (can be viewed in `./src/app/core/models/sensor.interfaces.ts`):

```
{
  "id": string, // UUID for this sensor reading
  "box_id": string, // UUID of the box
  "sensor_type": string, // type of the sensor
  "name": string, // type of data read by sensor
  "range_l": number, // measuring range lower bound
  "range_u": number, // measuring range upper bound
  "longitude": number, // location of the box (lon)
  "latitude": number, // location of the box (lat)
  "reading": number, // actual value being read
  "unit": string, // measurement unit
  "reading_ts": string // when the reading was taken
}
```

### Functionality

An Angular application has been created to do the following:

1. Reads the records from the sensor_readings.json
2. Displays them in a tabular component
3. Allows the user to sort data by time and sensor type
4. Allows the user to filter data by sensor type/name
5. Allows the user to see sensor location on a map (leveraging Leaflet)

## Code Layout
A high-level view of the code layout within the `./src` directory.

```
- app/
    |- components/
        |- map/                 | Component for displaying a single sensor box location on map
        |- table/               | Component for displaying sensor data in paginated tabulated format
            |- table-filter/    | Component for filter on table by sensory/type name
    |- core/
        |- models/              | Holds interfaces for the application
        |- services/            | Holds service for extracting sensor data from assets/data/sensor_readings.json file
        |- urls/                | Holds URL path for sensor data .json file 
        |- environments/        | Holds files which dictate whether to enable production mode or not
- assets/
    |- data/                    | Holds sensor_readings.json file
    |- i18n/                    | Holds translations for use by translate pipe (in this case it's used to translate technical column names into user-friendly names)
```

## Key design decisions

1. Streaming the JSON data into the application instead of rendering all of it at once through the use of Oboe, enabling the user to start viewing data before all of the data is rendered on the view. The benefits of this would be realised for larger amounts of data
2. Map is displayed in the background behind the overlying table. A couple of points to note with this:
    * It is created when the application starts up but is not displayed - this helps to save time when the user actually does select a sensor they want to view on the map
    * Leaflet methods (for converting between lat-lng and x-y pixels) and a listener for the size of the table-paginator wrapper have been used to ensure the offset is correct and ensures the marker is visible (i.e. not hidden behind the table or awkwardly positioned of the center of the visible area)
3. The application has been built to be responsive to difference device sizes and to be compatible with the major browsers

### Choice of Libraries

* [Oboe](https://www.npmjs.com/package/oboe)
    * **Summary**: Oboe.js is an open source Javascript library for loading JSON using streaming, combining the convenience of DOM with the speed and fluidity of SAX.
    * **Reason for use**: 
        * Sensor data provided in response follows a [line-delimited JSON](https://en.wikipedia.org/wiki/JSON_streaming) format.
        This provides an opportunity to stream the data into the application enabling the user to start viewing the results before everything has 
        been completely rendered, making the application appear more performant than if we were to load all of the data at once.
        * _Note: The difference is marginal for this particular application given the .json file is relatively small, but this approach would scale better if we were to expand the amount of sensor data_
        * The main alternative JSON streaming option could be to use `ndjson` package, however given Oboe offers superior documentation this option was preferred.
    * **Alternative options**: [ndjson](https://www.npmjs.com/package/ndjson), [ndjson-rxjs](https://www.npmjs.com/package/ndjson-rxjs)
* [Leaflet](https://leafletjs.com/)
    * **Summary**: Leading open-source Javascript library for interactive maps
    * **Reason for use**: A single map was required to display one marker at any given time. Due to requirement to display geospatial data, Leaflet was utilised. No major reason for choice other than previous experience :)
        * _Note: It is also possible to use [@asymmetrik/ngx-leaflet](https://www.npmjs.com/package/@asymmetrik/ngx-leaflet) for full Angular support (it wouldn't change much for this particular project however, given the simplicity of functionality required).
    * **Alternative options**: [openLayers](https://www.npmjs.com/package/ol), etc.
* [Angular Resize Event](https://www.npmjs.com/package/angular-resize-event)
    * **Summary**: Angular directive for detecting changes of an element size.
    * **Reason for use**: Lightweight package to aid in determining offset for marker placed on map to ensure it is always visible between the table and top of the application
* [Angular Animations Utility Library](https://www.npmjs.com/package/angular-animations)
    * **Summary**: Angular Animations utility library is a collection of reusable and parametrized animations build for Angular 4.4.6+ that can be used in a declarative manner
    * **Reason for use**: Simple to implement intriguing animations which are triggered and delayed to draw the user's attention to relevant components
* [ngx-translate](https://www.npmjs.com/package/@ngx-translate/core)
    * **Summary**: Define translations for content
    * **Reason for use**: Typically useful when converting the application into multiple different languages - used in this project for converting technical column names to user-friendly names, with the benefit for having the relevant conversions all in one place (i.e. `./assets/i18n`)
    * **Note**: Not necessary for this project, however this becomes an increasingly useful tool as an application scales to many users in different countries and/or as increasing numbers of new column names/data points are introduced
* [jasmine-marbles](https://www.npmjs.com/package/jasmine-marbles)
    * **Summary**: Marble testing helpers for RxJS and Jasmine
    * **Reason for use**: Useful for unit testing subscriptions feeding through streams of observables
* [Angular Material](https://material.angular.io/)
    * **Summary**: Material design components for Angular
    * **Reason for use**: Aids in speed of development for creating high quality and attractive looking components
    
### Opportunities for improving the application further

* The major area for improvement would be to ensure the pagination, sorting and filtering of the table scales better. As the amount of sensor data grows, it would be increasingly difficult to sort and filter the data within the frontend using the current approach (which relies on a BehaviorSubject containing an array of all the sensors). A better approach would be to:

    * Load the .json data into a database (e.g. PostgreSQL)
    * Expose a server side API which communicates directly with the database and feed sensor data to the front-end in a LDJSON format (same as current approach)
    * Use parameters in request from front-end to:
        * Fetch a stream of data for the first x number of pages
        * Accept parameters from frontend for filter, sort method (i.e. column_name and asc/desc), page size and current page user is on
    * When user navigates to a new page (outside of range of the x pages returned), a new HTTP request and stream of data is required
    * This approach would ensure the pagination, sorting and filtering capability scales better with large numbers of data points for the sensors, since it will have enough data to ensure a smooth user experience when flicking through pages
        * However, if the user did want to trawl through each page deep into the table, then an additional HTTP request can be triggered to fetch the additional data when they reach the page
        * Of course, this would be dependent on how many pages of the table a typical user would flick through per session (which would be something to track to determine if this approach would make sense)

## Developer Guide

### Running locally

Run `npm install` to install all dependencies for the project.

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

### Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

### Running unit tests

Unit tests have been written for every component and service.

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

### Running end-to-end tests

E2E test have been written to test the requirements listed in [Functionality](#functionality).

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
